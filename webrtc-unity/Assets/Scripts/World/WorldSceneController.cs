﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RTC;

public class WorldSceneController : MonoBehaviour {

    [SerializeField] RTCPresenter m_RTCPresenter;

	// Use this for initialization
	void Start () {
        Debug.Log("--- hello world ---");
#if UNITY_ANDROID && !UNITY_EDITOR
    AndroidJavaClass playerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    AndroidJavaObject activity = playerClass.GetStatic<AndroidJavaObject>("currentActivity");
    AndroidJavaClass utilityClass = new AndroidJavaClass("org.webrtc.UnityUtility");
    utilityClass.CallStatic("InitializePeerConncectionFactory", new object[1] { activity });
#endif
        m_RTCPresenter.Initialize();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
