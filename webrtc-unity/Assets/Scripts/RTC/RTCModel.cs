﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


namespace RTC
{
    public enum ConnectionState
    {
        none = -1,
        setup,
        signaling,
        connected,
        shutdown
    }

    public enum MediaType
    {
        none = -1,
        videoAudio,
        videoOnly,
        audioOnly,
    }

    public class RTCModel
    {
        public ReactiveProperty<ConnectionState> connectionState { get; private set; }
        public ReactiveProperty<MediaType> mediaType { get; private set; }

        public RTCModel()
        {
            Debug.Log("--rtc model intialized--");
            connectionState = new ReactiveProperty<ConnectionState>(ConnectionState.none);
            mediaType = new ReactiveProperty<MediaType>(MediaType.none);
        }

        public void ChangeConnectionState(ConnectionState next)
        {
            connectionState.Value = next;
        }

        public void ChangeMediaType(MediaType next)
        {
            mediaType.Value = next;
        }

    }
}

