﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using SimplePeerConnectionM;
using System;
using System.Runtime.InteropServices;
using WebSocketSharp;
//using WebSocketSharp.Server;

namespace RTC
{

    [Serializable]
    public class FirstArg
    {
        public string type;
    }

    [Serializable]
    public class SesstionDescription
    {
        public string type;
        public string sdp;
    }

    [Serializable]
    public class ICECandidate
    {
        public string type;
        public Candidate ice;
    }

    [Serializable]
    public class Candidate
    {
        public string candidate;
        public string sdpMid;
        public int sdpMLineIndex;
    }


    public class RTCPresenter : MonoBehaviour
    {
        [SerializeField] RTCView m_RTCView;
        [SerializeField] string websocketURL;
        public RTCModel m_RTCModel { get; private set; }
        private PeerConnectionM peer;
        private WebSocket ws;


        public void Initialize()
        {
            m_RTCModel = new RTCModel();
            m_RTCView.Initialize();

            m_RTCView.ConnetionButtonClickCallback += ConnectinoButtonClickCallback;
            m_RTCView.SendButtonClickCallback += SendButtonClickCallback;
            m_RTCView.OfferButtonClickCallback += OfferButtonClickCallback;
            m_RTCView.MicButtonClickCallback += MicButtonClickCallback;

            Bind();

        }

        private void OnDestroy()
        {
            m_RTCView.ConnetionButtonClickCallback -= ConnectinoButtonClickCallback;
            m_RTCView.SendButtonClickCallback -= SendButtonClickCallback;
            m_RTCView.OfferButtonClickCallback -= OfferButtonClickCallback;
            m_RTCView.MicButtonClickCallback -= MicButtonClickCallback;

            if (peer != null)
            {
                peer.ClosePeerConnection();
                
                peer = null;
            }
        }

        // ======= view callback =========
        void ConnectinoButtonClickCallback()
        {
            Debug.Log("connect button");
            m_RTCModel.ChangeConnectionState(ConnectionState.setup);
        }

        void OfferButtonClickCallback()
        {
            Debug.Log("offer button");
            m_RTCModel.ChangeConnectionState(ConnectionState.signaling);
            
            peer.CreateOffer();
        }

        void SendButtonClickCallback()
        {
            Debug.Log("send button");
            byte[] buff = new byte[sizeof(float)];
            float positionX = 3.123456789f;

            Buffer.BlockCopy(BitConverter.GetBytes(positionX), 0, buff, 0, sizeof(float));

            peer.SendBytesViaDataChannel(buff);

            peer.SendDataViaDataChannel("hello");
        }

        void MicButtonClickCallback()
        {
            m_RTCModel.ChangeMediaType(MediaType.audioOnly);
        }

        // ======= UniRX =======
        void Bind()
        {
            m_RTCModel.connectionState
                .Where(state => (state == ConnectionState.setup))
                .Subscribe(_ =>
                {
                    Debug.Log("state: " + _);
                    m_RTCView.ChangeStateToSetUp();
                    ws = SetupWebsocket();
                    ws.Connect();
                    peer = PrepareNewConnection();
                    if(m_RTCModel.mediaType.Value == MediaType.audioOnly)
                    {
                        peer.AddStream(true);
                        LogUtil.State("peer add stream auio only: " + true);
                    }
                    
                }).AddTo(this.gameObject);

            m_RTCModel.connectionState
                .ObserveOn(Scheduler.ThreadPool)
                .ObserveOnMainThread()
                .Where(state => (state == ConnectionState.connected))
                .Subscribe(_ =>
                {
                    Debug.Log("state: " + _);
                    m_RTCView.ChangeStateToConnected();
                }).AddTo(this.gameObject);

            m_RTCModel.connectionState
                .ObserveOn(Scheduler.ThreadPool)
                .ObserveOnMainThread()
                .Where(state => (state == ConnectionState.signaling))
                .Subscribe(_ =>
                {
                    Debug.Log("state: " + _);
                    m_RTCView.ChangeStateToSignaling();
                }).AddTo(this.gameObject);
        }

        // Websocket 
        WebSocket SetupWebsocket()
        {
            WebSocket _ws = new WebSocket(websocketURL);
            _ws.OnOpen += OnWebSocketOpen;
            _ws.OnMessage += OnWebSocketMessage;
            _ws.OnClose += OnWebSocketClose;
            _ws.OnError += OnWebSocketError;

            return _ws;
        }
        void OnWebSocketOpen(object sender, EventArgs eventArgs)
        {
            LogUtil.State("websocket open");
            FirstArg initialize = new FirstArg();
            initialize.type = "initialize";
            ws.Send(JsonUtility.ToJson(initialize));
        }
        void OnWebSocketMessage(object sender, MessageEventArgs e)
        {
            if(m_RTCModel.connectionState.Value == ConnectionState.connected)
            {
                LogUtil.State("already connected return");
            }

            var firstArg = JsonUtility.FromJson<FirstArg>(e.Data.ToString());

            if(peer == null)
            {
                LogUtil.State("peer not exist return");
                return;
            }

            switch(firstArg.type)
            {
                case "offer":
                    SesstionDescription offerMessage = JsonUtility.FromJson<SesstionDescription>(e.Data.ToString());
                    m_RTCModel.ChangeConnectionState(ConnectionState.signaling);

                    peer.SetRemoteDescription(offerMessage.type, offerMessage.sdp);
                    peer.CreateAnswer();
                    break;
                case "answer":
                    SesstionDescription answerMessage = JsonUtility.FromJson<SesstionDescription>(e.Data.ToString());

                    peer.SetRemoteDescription(answerMessage.type, answerMessage.sdp);
                    break;
                case "candidate":
                    ICECandidate candidateMessage = JsonUtility.FromJson<ICECandidate>(e.Data.ToString());

                    peer.AddIceCandidate(candidateMessage.ice.candidate, candidateMessage.ice.sdpMLineIndex, candidateMessage.ice.sdpMid);
                    break;

            }

        }
        void OnWebSocketError(object sender, EventArgs eventArges)
        {
            LogUtil.State("websocket on error");
        }
        void OnWebSocketClose(object sender, EventArgs eventArgs)
        {
            LogUtil.State("websocket on close");
        }

        // WebSocket Functions
        void SendSDP(string stringSDP)
        {
            if(ws == null)
            {
                LogUtil.State("WebSocket does not exist return");
                return;
            }

            ws.Send(stringSDP);
        }

        // Peerconnection
        PeerConnectionM PrepareNewConnection()
        {
            List<string> servers = new List<string>();
            servers.Add("stun: stun.skyway.io:3478");
            servers.Add("stun: stun.l.google.com:19302");
            PeerConnectionM _peer = new PeerConnectionM(servers, "", "");
            int id = _peer.GetUniqueId();
            Debug.Log("PeerConnectionM.GetUniqueId() : " + id);

            _peer.OnLocalSdpReadytoSend += OnLocalSessionDescriptionReadytoSend;
            _peer.OnIceCandiateReadytoSend += OnICECandidateReadyToSend;
            _peer.OnLocalVideoFrameReady += OnI420LocalFrameReady;
            _peer.OnRemoteVideoFrameReady += OnI420RemoteFrameReady;
            _peer.OnAudioBusReady += OnAudioBusReady;
            _peer.OnLocalDataChannelReady += OnLocalDataChannelReady;
            _peer.OnDataFromDataChannelReady += OnDataFromDataChannel;
            _peer.OnBytesFromDataChannelReady += OnBytesFromDataChannel;

            _peer.OnFailureMessage += OnFailuerMessage;
            _peer.AddDataChannel();

            return _peer;
        }

        // Datachannel
        void OnLocalDataChannelReady(int id)
        {
            Debug.Log("===== data channel ready ======");
            m_RTCModel.ChangeConnectionState(ConnectionState.connected);
        }

        void OnDataFromDataChannel(int id, string s)
        {
            Debug.Log("data channel message: " + s);
        }

        void OnBytesFromDataChannel(int id, IntPtr bytes, int bytesLength)
        {
            Debug.Log("bytes recieved sizes: " + bytesLength);
            byte[] buffer = new byte[bytesLength];
            Marshal.Copy(bytes, buffer, 0, bytesLength);
            float value = BitConverter.ToSingle(buffer, 0);
            Debug.Log("value: " + value);
        }


        // ======= peerconnection callbacks =======
        void OnLocalSessionDescriptionReadytoSend(int id, string type, string sdp)
        {
            switch (type)
            {
                case "offer":
                case "answer":
                    LogUtil.State("local answer");
                    SesstionDescription sessionDescription = new SesstionDescription();
                    sessionDescription.type = type;
                    sessionDescription.sdp = sdp;
                    string messageString = JsonUtility.ToJson(sessionDescription);

                    if (peer == null)
                    {
                        LogUtil.State("peer does not exist return");
                        return;
                    }
                    
                    SendSDP(messageString);
                    break;
                default:
                    Debug.Log("unknown type:" + type);
                    break;
            }
        }
        void OnICECandidateReadyToSend(int id, string candidate, int sdpMlineIndex, string sdpMid)
        {
            Candidate candidateSDP = new Candidate();
            candidateSDP.candidate = candidate;
            candidateSDP.sdpMLineIndex = sdpMlineIndex;
            candidateSDP.sdpMid = sdpMid;
            ICECandidate icecandidate = new ICECandidate();
            icecandidate.type = "candidate";
            icecandidate.ice = candidateSDP;

            var message = JsonUtility.ToJson(icecandidate);
            SendSDP(message);
        }
        void OnI420LocalFrameReady(int id,
            IntPtr dataY, IntPtr dataU, IntPtr dataV,
            int strideY, int strideU, int strideV,
            uint width, uint height)
        {

        }
        void OnI420RemoteFrameReady(int id,
            IntPtr dataY, IntPtr dataU, IntPtr dataV,
            int strideY, int strideU, int strideV,
            uint width, uint height)
        {

        }
        void OnAudioBusReady(int id, IntPtr data, int bitsPerSample,
            int sampleRate, int numberOfChannels, int numberOfFrames)
        {
            LogUtil.State("on audio bus ready");
        }

        void OnFailuerMessage(int id, string msg)
        {
            LogUtil.State("failed: " + msg);
            m_RTCModel.ChangeConnectionState(ConnectionState.shutdown);
        }

    }
}