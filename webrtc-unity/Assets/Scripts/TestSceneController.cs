﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimplePeerConnectionM;

public class TestSceneController : MonoBehaviour {

    PeerConnectionM peer;
	// Use this for initialization
	void Start () {
        List<string> servers = new List<string>();
        servers.Add("stun: stun.skyway.io:3478");
        servers.Add("stun: stun.l.google.com:19302");
        peer = new PeerConnectionM(servers, "", "");
        int id = peer.GetUniqueId();
        Debug.Log("PeerConnectionM.GetUniqueId() : " + id);

    }


}
