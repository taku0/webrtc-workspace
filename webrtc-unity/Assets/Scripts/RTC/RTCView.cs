﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace RTC
{
    public class RTCView : MonoBehaviour
    {
        public Action ConnetionButtonClickCallback;
        public Action SendButtonClickCallback;
        public Action OfferButtonClickCallback;
        public Action MicButtonClickCallback;

        [SerializeField] Button ConnectButton;
        [SerializeField] Button SendButton;
        [SerializeField] Button OfferButton;
        [SerializeField] Button MicButton;

        public void Initialize()
        {
            Debug.Log("--rtc view init--");
        }

        public void OnConnectButtonClicked()
        {
            if (ConnetionButtonClickCallback != null)
            {
                ConnetionButtonClickCallback();
            }
        }

        public void OnSendButtonClick()
        {
            if (SendButtonClickCallback != null)
            {
                SendButtonClickCallback();
            }
        }

        public void OnOfferButtonClick()
        {
            if (OfferButtonClickCallback != null)
            {
                OfferButtonClickCallback();
            }
        }

        public void OnMicButtoinClick()
        {
            if (MicButtonClickCallback != null)
            {
                MicButtonClickCallback();
            }
        }

        public void ChangeStateToSetUp()
        {
            ConnectButton.gameObject.SetActive(false);
            SendButton.gameObject.SetActive(false);
            OfferButton.gameObject.SetActive(true);
            MicButton.gameObject.SetActive(false);
        }

        public void ChangeStateToConnected()
        {
            ConnectButton.gameObject.SetActive(false);
            SendButton.gameObject.SetActive(true);
            OfferButton.gameObject.SetActive(false);
            MicButton.gameObject.SetActive(false);
        }

        public void ChangeStateToSignaling()
        {
            ConnectButton.gameObject.SetActive(false);
            SendButton.gameObject.SetActive(false);
            OfferButton.gameObject.SetActive(false);
            MicButton.gameObject.SetActive(false);
        }
    }
}