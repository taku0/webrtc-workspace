const WebRTC = require('./webrtc-client');

let ws, webrtc;
let message_queue;
let bytes_queue;
let videoElement;

window.onload = function () {
    videoElement = document.createElement('video');
    document.body.appendChild(videoElement);
    videoElement.volume = 0;
    main();
}

async function main() {
    webrtc = new WebRTC();
    webrtc.sendSDP = sendSDP;
    webrtc.sendICECandidate = sendICECandidate;
    webrtc.OnDataChannelMessage = OnDataChannelMessage;
    webrtc.OnAddRemoteStreamCallback = OnAddRemoteStream;
    webrtc.OnRemoveStreamCallback = OnRemoveStram;
    await webrtc.startVideo(videoElement);
    //const urlstr = 'ws://192.168.1.27:3333';//'ws://localhost:3000';
    setupWS(/*urlstr*/);

    const connectButton = document.createElement('button');
    connectButton.addEventListener('click', async function (event) { await connectAsync() });
    document.body.appendChild(connectButton);
}

const sendSDP = function (sessionDescription) {
    const message = JSON.stringify(sessionDescription);
    ws.send(message);
}

const sendICECandidate = function (candidate) {
    const message = JSON.stringify({ type: 'candidate', ice: candidate });
    ws.send(message);
}

const OnDataChannelOpen = function () {
    console.log("==== data channel on open ====");
}

const OnDataChannelMessage = function (message) {
    const data = message.data;
    if (typeof (data) === 'string') {
        console.log('string message: ', data);
        message_queue = data;
    } else {
        let dataview = new DataView(data);
        let valueF = dataview.getFloat32(0, true);
        console.log("bytes value: ", valueF);
        bytes_queue = data;
    }
}

const OnAddRemoteStream = function (stream, id) {
    // console.log("==== on add remote stream ====");
    console.log("stream: ", stream.id);

    const video = document.createElement('video');
    video.id = stream.id;
    video.srcObject = stream;
    video.volume = 1;
    video.play();
    document.body.appendChild(video);
}

const OnRemoveStram = function (stream) {
    console.log(stream);
    const videoEl = document.getElementById(stream.id);
    document.body.removeChild(videoEl);

};

async function connectAsync() {
    await webrtc.makeOfferAsync();
}

function sendMessage(message) {
    console.log(webrtc._dataChannel);
    webrtc._dataChannel.send(message);
}

function sendBackString() {
    if (message_queue) {
        webrtc._dataChannel.send(message_queue);
        console.log(message_queue);
    } else {
        console.log("---- message_queue null ----");
    }
}

function sendBackBytes() {
    if (bytes_queue) {
        webrtc._dataChannel.send(bytes_queue);
        console.log(bytes_queue);
    } else {
        console.log('---- bytes_queue null ----');
    }
}

function setupWS() {
    const local = 'ws://localhost:3002';
    ws = new WebSocket(local);

    ws.onopen = function () {
        console.log('on open');
        const message = { type: 'initialize' };
        ws.send(JSON.stringify(message));
    };
    ws.onmessage = async function (evt) {
        // console.log('ws onmessage() data:', evt.data);
        const message = JSON.parse(evt.data);
        // console.log('recieved message: ' + message.type);
        switch (message.type) {
            case 'offer': {
                console.log('Received offer ...');
                await webrtc.setOfferAsync(message);
                break;
            }
            case 'answer': {
                console.log('Received answer ...');
                await webrtc.setAnswerAsync(message);
                break;
            }
            case 'candidate': {
                console.log('Received ICE candidate ...');
                if (message.ice) {
                    const candidate = new RTCIceCandidate(message.ice);
                    webrtc.addIceCandidate(candidate);
                }
                break;
            }
            case 'room': {
                console.log('Received Room id ...');
                break;
            }
            case 'close': {
                console.log('peer is closed ...');
                webrtc.hangUp();
                break;
            }
            default: {
                console.log("Invalid message");
                break;
            }
        }
    }
}